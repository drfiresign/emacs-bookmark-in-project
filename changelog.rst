
##########
Change Log
##########

- In development.

  - Fix bug inserting bookmarks into empty files.

- Version 0.1 (2022-01-02)

  Initial release.
